import React from 'react';
import './App.css';

// const RESULT = [
//   {
//     title: 'React',
//     url: 'https://facebook.github.io/react/',
//     author: 'Jordan Walke',
//     num_comments: 3,
//     points: 4,
//     objectID: 0,
//   },
//   {
//     title: 'Redux',
//     url: 'https://github.com/reactjs/redux',
//     author: 'Dan Abramov, Andrew Clark',
//     num_comments: 2,
//     points: 5,
//     objectID: 1,
//   },
// ];

const PATH_BASE = "https://hn.algolia.com/api/v1";
const PATH_SEARCH = "/search";
const PARAM_QUERY = "query=";
const PARAM_PAGE = "page=";
const PARAM_HPP = "hitsPerPage=";

const DEFAULT_QUERY = "redux";
const DEFAULT_HPP = "5";

function Button(props) {
  const {children, onButtonClick} = props;

  return (
    <button
      type="text"
      onClick={onButtonClick}
    >
      {children}
    </button>
  );
}

function ResultRow({item, renderDismissButton}) {
  return (
    <tr>
      <td><a href={item.url}>{item.title}</a></td>
      <td>{item.author}</td>
      <td>{item.num_comments}</td>
      <td>{item.points}</td>
      <td>{renderDismissButton(item.objectID)}</td>
    </tr>
  );
}

function ResultTable({result, ...props}) {
  const rows = [];

  //this was for filtering local results, but was replaced 
  // const rows = result.reduce((accumulator, item) => {
  //   if(item.title.toLowerCase().indexOf(props.searchTerm.toLowerCase()) === -1) {
  //     return accumulator;
  //   }

  //   accumulator.push(
  //     <ResultRow 
  //       key={item.objectID} 
  //       item={item} 
  //       onDismiss={handleDismiss}
  //     />
  //   );
  //   return accumulator;
  // }, []); 

  for (let item of result) {
    rows.push(
      <ResultRow
        key={item.title}
        item={item}
        {...props}
      />
    );
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>Comments</th>
          <th>Points</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </table>
  );
}

function SearchBar(props) {
  const {
    searchTerm,
    children,
    onSearchChange,
    onSearchSubmit
  } = props;

  const handleSearchChange = (event) => onSearchChange(event.target.value);
  const handleSearchSubmit = (event) => onSearchSubmit(event);

  return (
    <form onSubmit={handleSearchSubmit}>
      <input
        type="text"
        value={searchTerm}
        onChange={handleSearchChange}
      />
      <input 
        type="submit"
        value={children}
      />
    </form>
  );
}

class FilterableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {searchTerm: DEFAULT_QUERY, results: null, resultKey: ""};
    this.handleDismiss = this.handleDismiss.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.fetchSearchResult = this.fetchSearchResult.bind(this);
    this.setSearchResult = this.setSearchResult.bind(this);
  }

  handleDismiss(objectID) {
    const {results, resultKey} = this.state;
    const result = results[resultKey];
    const filteredHits = result.hits.filter(hit => hit.objectID !== objectID)
    const filteredResults = {...result, hits: filteredHits};
    this.setState({results: {...results, [resultKey]: filteredResults}});
  }

  handleSearchChange(searchTerm) {
    this.setState({searchTerm});
  }

  handleSearchSubmit(event) {
    const {searchTerm, results} = this.state;

    this.setState({resultKey: searchTerm});

    if(!results[searchTerm]) {
      this.fetchSearchResult(searchTerm);
    }

    event.preventDefault();
  }

  componentDidMount() {
    const {searchTerm} = this.state;

    this.setState({resultKey: searchTerm});
    this.fetchSearchResult(searchTerm);
  }

  fetchSearchResult(searchTerm, page=0) {
    const URL = `${PATH_BASE}${PATH_SEARCH}?${PARAM_QUERY}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}` 
    fetch(URL)
    .then(response => response.json())
    .then(result => this.setSearchResult(result))
    .catch(error => error);
  }

  setSearchResult(result) {
    const {results, resultKey} = this.state;
    const {hits, ...leftoverResult} = result;

    const prevHits = (results && results[resultKey] && results[resultKey].hits) || [];
    const updatedResult = {...leftoverResult, hits: [...prevHits, ...hits]};
    this.setState({results: {...results, [resultKey]: updatedResult}});
  }

  render() {
    const {searchTerm, results, resultKey} = this.state;
    const hits = (results && results[resultKey] && results[resultKey].hits) || [];
    const page = (results && results[resultKey] && results[resultKey].page) || 0;

    console.log(results);

    return (
      <div>
        <SearchBar 
          searchTerm={searchTerm}
          onSearchChange={this.handleSearchChange}
          onSearchSubmit={this.handleSearchSubmit}
        >
          Search
        </SearchBar>
        <ResultTable
          result={hits}
          renderDismissButton={objectID => 
            <Button 
              onButtonClick={() => this.handleDismiss(objectID)}
            >
              Dismiss
            </Button>}
        />
        <Button 
          onButtonClick={() => this.fetchSearchResult(searchTerm, page+1)}
        >
          More
        </Button>
      </div>
    );
  }
}

function App() {
  return (
    <FilterableTable />
  );
}

export default App;
