import React, {Component} from 'react';
import axios from 'axios';
import { sortBy } from 'lodash';
import './App.css';

const PATH_BASE = "https://hn.algolia.com/api/v1";
const PATH_SEARCH = "/search";
const PARAM_SEARCH = "query=";
const DEFAULT_QUERY = "redux";
const PARAM_PAGE = "page=";
const PARAM_HPP = "hitsPerPage="
const DEFAULT_HPP = "25";

const SORTS = {
  NONE: list => list,
  TITLE: list => sortBy(list, 'title'),
  AUTHOR: list => sortBy(list, 'author'),
  COMMENTS: list => sortBy(list, 'num_comments').reverse(),
  POINTS: list => sortBy(list, 'points').reverse(),
};

// const url = `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${DEFAULT_QUERY}`;

// const list = [
//   {
//     title: 'React',
//     url: 'https://facebook.github.io/react/',
//     author: 'Jordan Walke',
//     num_comments: 3,
//     points: 4,
//     objectID: 0,
//   },
//   {
//     title: 'Redux',
//     url: 'https://github.com/reactjs/redux',
//     author: 'Dan Abramov, Andrew Clark',
//     num_comments: 2,
//     points: 5,
//     objectID: 1,
//   }
// ];

const largeColumn = {
  width: '40%',
};
const midColumn = {
  width: '30%',
};
const smallColumn = {
  width: '10%',
};

// const isSearched = searchTerm => 
//   item => item.title.toLowerCase().includes(searchTerm.toLowerCase());

const Search = ({ onChange, onSubmit, value, children }) => 
  <form onSubmit={onSubmit}>
    <input onChange={onChange} type="text" value={value} />
    <button type="submit">{children}</button>
  </form>

// const Table = ({ onClick, value, list }) =>
// <div className="table">
//   {list.filter(isSearched(value)).map(item =>
const Table = ({ onClick, onSort, sortKey, isSortReverse, list }) => {
  const sortedList = isSortReverse ? SORTS[sortKey](list).reverse() : SORTS[sortKey](list);
  return (
    <div className="table">
      <div className="table-header">
        <span style={largeColumn}>
          <Sort onSort={onSort} sortKey={"TITLE"} activeSortKey={sortKey}>
            Title
        </Sort>
        </span>
        <span style={midColumn}>
          <Sort onSort={onSort} sortKey={"AUTHOR"} activeSortKey={sortKey}>
            Author
        </Sort>
        </span>
        <span style={smallColumn}>
          <Sort onSort={onSort} sortKey={"COMMENTS"} activeSortKey={sortKey}>
            Comments
        </Sort>
        </span>
        <span style={smallColumn}>
          <Sort onSort={onSort} sortKey={"POINTS"} activeSortKey={sortKey}>
            Points
        </Sort>
        </span>
        <span style={smallColumn}>
          Archive
      </span>
      </div>
      {sortedList.map(item =>
        <div key={item.objectID} className="table-row">
          <span style={largeColumn}><a href={item.url}>{item.title}</a></span>
          <span style={midColumn}>{item.author}</span>
          <span style={smallColumn}>{item.num_comments}</span>
          <span style={smallColumn}>{item.points}</span>
          <span style={smallColumn}>
            <Button onClick={() => onClick(item.objectID)} className="button-inline">
              Dismiss
          </Button>
          </span>
        </div>
      )}
    </div>
  );
}

const Button = ({ onClick, className = "", children }) =>
  <button onClick={onClick} className={className} type="button">
    {children}
  </button>

const Loading = () => <div>Loading...</div>

const withLoading = (Component) => ({ isLoading, ...rest }) =>
      isLoading
      ? <Loading />
      : <Component { ...rest } />

const ButtonWithLoading = withLoading(Button);

const Sort = ({ onSort, sortKey, activeSortKey, children }) => {
  const sortClass = sortKey === activeSortKey ? "button-inline button-active" : "button-inline";

  return (
    <Button onClick={() => onSort(sortKey)} className={sortClass}>
      {children}
    </Button>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = { 
      results: null,
      resultKey: "",
      searchTerm: DEFAULT_QUERY,
      error: null,
      isLoading: false,
      sortKey: 'NONE',
      isSortReverse: false
    };

    this.setSearchTopStories = this.setSearchTopStories.bind(this);
    this.getSearchTopStories = this.getSearchTopStories.bind(this);
  }

  componentDidMount() {
    this.setState({ resultKey: this.state.searchTerm });
    this.getSearchTopStories(this.state.searchTerm);
  }

  getSearchTopStories(searchTerm, page = 0) {
    this.setState({ isLoading: true });
    
    axios(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}` +
          `&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
      .then(result => this.setSearchTopStories(result.data))
      .catch(error => this.setState({ error }));

    // fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}` +
    //       `&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
    //   .then(response => response.json())
    //   .then(result => this.setSearchTopStories(result))
    //   .catch(error => this.setState({ error }));
  }

  setSearchTopStories(result) {
    this.setState(prevState => {
      const oldList = prevState.results && prevState.results[prevState.resultKey]
        ? prevState.results[prevState.resultKey].hits
        : [];

      const newList = [...oldList, ...result.hits];

      return {
        results: Object.assign(
          {},
          prevState.results,
          { [prevState.resultKey]: { hits: newList, page: result.page } }),
        isLoading: false
      }
    });

    // const oldList = this.state.results && this.state.results[this.state.resultKey]
    //   ? this.state.results[this.state.resultKey].hits
    //   : [];

    // const newList = [...oldList, ...result.hits];

    // this.setState({
    //   results: Object.assign(
    //     {},
    //     this.state.results,
    //     { [this.state.resultKey]: { hits: newList, page: result.page } }),
    //   isLoading: false
    // });
  }

  needToSearch = (searchTerm) => !this.state.results[searchTerm];

  onDismiss = (id) => {
    const updatedList = this.state.results[this.state.resultKey].hits.filter(item => id !== item.objectID);
    this.setState({ results: Object.assign(
      {}, 
      this.state.results, 
      { [this.state.resultKey]: { hits: updatedList, page: this.state.results[this.state.resultKey].page } }) 
    });
  };

  onSearchChange = (event) => {
    this.setState({ searchTerm: event.target.value });
    //console.log(e);
  };

  onSearchSubmit = (event) => {
    this.setState({ resultKey: this.state.searchTerm });
    
    if(this.needToSearch(this.state.searchTerm)) this.getSearchTopStories(this.state.searchTerm);
    
    event.preventDefault();
  };

  onSort = (sortKey) => {
    const isSortReverse = sortKey === this.state.sortKey && !this.state.isSortReverse;
    this.setState({ sortKey, isSortReverse });
  }

  render() {
    console.log(this.state.results);
    //if(!this.state.result) return null;

    const page = (this.state.results 
        && this.state.results[this.state.resultKey]
        && this.state.results[this.state.resultKey].page) || 0;
    
    const list = (this.state.results
      && this.state.results[this.state.resultKey]
      && this.state.results[this.state.resultKey].hits) || [];

    return (
      <div className="page">

        <div className="interactions">
          <Search
            onChange={this.onSearchChange}
            onSubmit={this.onSearchSubmit}
            value={this.state.searchTerm}
          >
            Search
          </Search>
        </div>

        {this.state.error
          ? <div className="interactions"><p>Something went wrong</p></div>
          : <Table
            onClick={this.onDismiss}
            onSort = {this.onSort}
            sortKey = {this.state.sortKey}
            isSortReverse = {this.state.isSortReverse}
            //value={this.state.searchTerm}
            list={list}
          />
        }

        <div className="interactions">
          {/*this.state.isLoading
            ? <Loading />
            : <Button onClick={() => this.getSearchTopStories(this.state.resultKey, page + 1)}>
              More
              </Button>
          */}
          <ButtonWithLoading 
            isLoading={ this.state.isLoading }
            onClick={() => this.getSearchTopStories(this.state.resultKey, page + 1)}>
            Load More
          </ButtonWithLoading>
        </div>

        {/* [<p key="one">paragraph one</p>, <p key="two">paragraph two</p>] */}

      </div>
    );
  }
}

export default App;
export {Search, Table, Button};
